﻿namespace AOSharp.Common.GameData
{
    public enum PerkHash
    {
        //Doc LE Procs
        MuscleMemory = 0x4E464C41,
        AnatomicBlight = 0x54484F47,
        RestrictiveBandaging = 0x5450464F,
        Pathogen = 0x43494353,
        BloodTransfusion = 0x444E4355,
        Anesthetic = 0x4B4C4650,
        Astringent = 0x4D4D4154,
        Inflammation = 0x5253424E,
        Antiseptic = 0x41565752,
        HealingCare = 0x5951545A,

        //Keeper LE procs
        BenevolentBarrier = 0x52495354,
        FaithfulReconstruction = 0x5357544F,
        HonorRestored = 0x434E4445,
        IgnoretheUnrepentant = 0x45545455,
        Subjugation = 0x5248424F,
        PureStrike = 0x45535455,
        EschewtheFaithless = 0x445A5455,
        RighteousStrike = 0x46545345,
        RighteousSmite = 0x46465245,
        AmbientPurification = 0x46454955,
        SymbioticBypass = 0x5249534D,
        VirtuousReaper = 0x46485245,

        //ENF LE Procs -- some missing
        IgnorePain = 0x49475049,
        TearLigaments = 0x42415353,
        VileRage = 0x49534952,
        ShrugOffHits = 0x564E534D,
        BustKneecaps = 0x58435346,
        ViolationBuffer = 0x42554B45,
        ShieldOfTheOgre = 0x494E48,
        InspireRage = 0x49535247,

        //MA LE Procs -- some missing
        DebilitatingStrike = 0x44425354,
        HealingMeditation = 0x484C4D44,
        DisruptKi = 0x44524B49,
        StingingFist = 0x53544653,
        StrengthenSpirit = 0x4154484D,
        AttackLigaments = 0x41544C49,
        MedicinalRemedy = 0x4D45524D,
        SmashingFist = 0x53484653,
        StrengthenKi = 0x53544B49,

        //NT LE Procs -- some missing
        PoweredNanoFortress = 0x54524548,
        UnstableLibrary = 0x44535245,
        AcceleratedReality = 0x45534C50,
        LayeredAmnesty = 0x5348524C,
        SourceTap = 0x51505357,
        IncreaseMomentum = 0x49434D4F,
        CircularLogic = 0x50475354,
        HarvestEnergy = 0x43494C4F,
        LoopingService = 0x55534C49,

        //Shade LE Proc -- some missing
        DrainEssence = 0x44524553,
        SapLife = 0x53504C49,
        ElusiveSpirit = 0x45555352,
        ToxicConfusion = 0x4F4E4655,
        ConcealedSurprise = 0x48525356,
        Misdirection = 0x53444952,
        DeviousSpirit = 0x44565350,
        TwistedCaress = 0x504C534E,
        SiphonBeing = 0x53504247,
        ShadowedGift = 0x53414746,

        //Champion of Nano Combat
        BotConfinement = 0x59475854,
        NanoFeast = 0x4E4E4641,

        //Champion of Light Infantry
        Bore = 0x4B4C564F,
        Crave = 0x414F514F,

        //Xuyun
        RedDusk = 0x52454455,
        Moonmist = 0x4F4F4E4D,
        RedDawn = 0x52454457,

        //Spiritual Master
        Obliterate = 0x4F424C49,
        FleshQuiver = 0x464C5149,
        Incapacitate = 0x494E4341,
        ChiConductor = 0x43494344,
        Dragonfire = 0x5241474F,

        //Kung-Fu Master
        HarmonizeBodyAndMind = 0x58484A55,
        TremorHand = 0x5452484E,

        //Disharmony
        BalanceOfYinAndYang = 0x42414F59,

        //Care in Battle
        EvasiveStance = 0x45415354,

        //Acrobat
        DanceOfFools = 0x44414F46,
        Limber = 0x4C494D42,

        //The Unknown Factor
        ChaoticAssumption = 0x43544155,
        HostileTakeover = 0x484F5441,

        //Ancient Matrix
        ProgramOverload = 0x50524F56,
        Utilize = 0x5554494C,
        FlimFocus = 0x464C4643,

        //Starfall
        Supernova = 0x5045524E,
        ThermalDetonation = 0x54454454,
        Combust = 0x5752544B,
        DazzleWithLights = 0x445A5749,

        //Notum Source
        AccessNotumSource = 0x41434E4F,

        //Notum Siphon
        TapNotumSource = 0x54414E54,     
        FadeAnger = 0x46414147,

        //Nano Doctorate
        ShutdownRemoval = 0x53555245,
        SkillDrainRemoval = 0x5348524D,

        //Essence of Notum
        NanoHeal = 0x5441484C,

        //Enhanced Nano Damage
        BreachDefenses = 0x42524445,
        
        //Shadow
        ChaosRitual = 0x4D4B4F50,
        Diffuse = 0x44494646,
        Blur = 0x4A4A524D,

        //Piercing Mastery
        Hecatomb = 0x45434154,
        Gore = 0x4B415750,
        Impale = 0x494D5041,
        Lacerate = 0x4C414345,
        Perforate = 0x50455246,
        DoubleStab = 0x444F5341,
        Stab = 0x4958554C,

        //Totemic Rites
        RitualOfBlood = 0x52544F42,
        DevourVitality = 0x444F5649,
        RitualOfSpirit = 0x52554F53,
        DevourEssence = 0x44454553,
        RitualOfZeal = 0x52544F5A,
        DevourVigor = 0x44455649,
        RitualOfDevotion = 0x524C4F44,

        //Spirit Phylactery
        CaptureVitality = 0x43545649,
        UnsealedContagion = 0x554E4347,
        CaptureSpirit = 0x43505349,
        UnsealedPestilence = 0x554E5053,
        CaptureEssence = 0x43504553,
        UnsealedBlight = 0x5541424C,
        CaptureVigor = 0x43505649,

        //Genius
        RegainNano = 0x52474E4E,

        //Assault Force Medic
        BattleGroupHeal1 = 0x42544841,
        BattleGroupHeal2 = 0x4241484C,
        BattleGroupHeal3 = 0x42544845,
        BattleGroupHeal4 = 0x42544834,

        //Specialist Healer
        HaleAndHearty = 0x5445484C,
 
        //Heavy Ranged
        LaserPaintTarget = 0x4C415049,
        WeaponBash = 0x57454253,
        TriangulateTarget = 0x54525441,
        NapalmSpray = 0x4E505352,

        //Special Forces
        FieldBandage = 0x4645424E,
        Tracer = 0x54524143,
        ContainedBurst = 0x434E4255,
        Violence = 0x45454544,
        Guardian = 0x41524449,

        //Power In Numbers
        SupressiveHorde = 0x5350484F,
        ClipFever = 0x434C4645,
        MuzzleOverload = 0x4D5A4F56,

        //Champion Of Heavy Infantry
        Fuzz = 0x4358575A,
        FireFrenzy = 0x46524652,

        //Soldier LE
        FuriousAmmunition = 0x494E414D,
        GrazeJugularVein = 0x52544D4F,

        //Power Up
        PowerCombo = 0x5057434F,
        PowerBlast = 0x504F424C,
        PowerShock = 0x504F5348,
        PowerVolley = 0x504F564F,
        Energize = 0x45524749,

        //LoopHole
        Antitrust = 0x4E544954,
        Puppeteer = 0x50555050,

        //Pistol Mastery
        Deadeye = 0x4E4F534D,
        DoubleShot = 0x44425348,
        QuickShot = 0x51495348,

        //SMG Mastery
        NeutroniumSlug = 0x4E45534C,
        SolidSlug = 0x534C5355,
        JarringBurst = 0x4A414255,
        ReinforceSlugs = 0x5245534C,

        //Crusade
        Purify = 0x554B4746,
        Insight = 0x4A475447,
        ForceOpponent = 0x464F4F50,

        //Cybernetic Samurai
        SeppukuSlash = 0x514A4F50,
        HonoringTheAncients = 0x484F5448,
        BladeWhirlwind = 0x424C5752,
        DeepCuts = 0x44454354,

        //Holy Mark
        MarkOfTheUnclean = 0x4D4B4F48,
        MarkOfSufferance = 0x4D414F53,
        MarkOfVengeance = 0x4D4B4F56,

        //Blessing
        CuringTouch = 0x4355544F,
        DevotionalArmor = 0x4445414D,
        LayOnHands = 0x4C414f4E,

        //Champion of Heavy Infantry
        Bluntness = 0x55535648,

        //Bio Shield
        BioRegrowth = 0x42495247,
        BioRejuvenation = 0x4249524A,
        BioCocoon = 0x42494343,
        BioShield = 0x42495349,

        //Bone Crusher (2hb)
        SeismicSmash = 0x534D534D,
        OverwhelmingMight = 0x4F454D47,
        HammerAndAnvil = 0x484D4144,
        Pulverize = 0x50554C56,

        //MannersOfMongo
        GroinKick = 0x474F4B4B,
        Hatred = 0x4E4D5754,
        Headbutt = 0x54585844,
        Charge = 0x42585558,
        Taunt = 0x47445A48,

        //Form of Troll
        Avalanche = 0x4156414C,
        StoneFist = 0x534E4649,
        DisableNaturalHealing = 0x44494E54,
        TrollForm = 0x5452464F,

        //Reaver
        SliceandDice = 0x53414149,
        PainLance = 0x50414C41,
        Transfix = 0x52414E53,
        Cleave = 0x434C4541,

        //Solitus Primary Genome
        TackyHack = 0x54414843,

        //Mutate
        CauseofAnger = 0x43534F41,
        ArouseAnger = 0x4155414E,

        //Colossal Health
        Lifeblood = 0x4C494645,
        BlessingofLife = 0x424E4F49,

        //Nano Surgeon
        TreatmentTransfer = 0x54525452,
        MaliciousProhibition = 0x4D41504F,
        TeamHeal = 0x54414841,
        EnhancedHeal = 0x45484841,

        //Embrace
        Mistreatment = 0x52454154,
    }
}
