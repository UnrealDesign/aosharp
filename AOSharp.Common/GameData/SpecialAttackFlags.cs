﻿namespace AOSharp.Common.GameData
{
    public enum SpecialAttackFlags
    {
        //TODO: holy shit fix this...
        Burst = 2048,
        FlingShot = 4096,
        FullAuto = 8192,
        AimedShot = 16384,
        SneakAttack = 131072,
        FastAttack = 262144,
        Brawl = 33554432,
        Dimach = 67108864
    }
}
