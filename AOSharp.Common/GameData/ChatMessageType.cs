﻿namespace AOSharp.Common.GameData
{
    public enum ChatMessageType : byte
    {
        Say = 0x00,
        Whisper = 0x01,
        Shout = 0x02
    }
}