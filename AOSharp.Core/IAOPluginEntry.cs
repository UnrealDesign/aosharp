﻿namespace AOSharp.Core
{
    public interface IAOPluginEntry
    {
        void Run(string pluginDir);
    }
}
