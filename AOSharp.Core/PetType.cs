﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOSharp.Core
{
    public enum PetType
    {
        Attack = 0xA,
        Heal = 0xB,
        Support = 0xC
    }
}
