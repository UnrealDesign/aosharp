﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;

namespace StatIncreaser
{
    public class Main : IAOPluginEntry
    {
        public void Run(string pluginDir)
        {
            try
            {
                Chat.WriteLine("Stat helper loaded");
                Chat.RegisterCommand("sb", dupeStats);
                Chat.RegisterCommand("sb2", dupeStats2);
                Chat.RegisterCommand("t3", moveToBackpack);

                /*
                Item noviRing;
                if (Inventory.Find(202742, out noviRing))
                {
                    noviRing.Equip(EquipSlot.Cloth_RightFinger);

                    Container openBag = Inventory.Backpacks.FirstOrDefault(x => x.IsOpen);
                    if (openBag != null)
                    {
                        noviRing.MoveToContainer(openBag);
                        Chat.WriteLine("Moved to backpack");
                    }
                }
                */
            }
            catch (Exception e)
            {
                Chat.WriteLine($"ERROR: {e.Message}");
            }
        }

        public void moveToBackpack(string command, string[] args, ChatWindow window)
        {
            if (Inventory.Find(202742, out Item noviRing))
            {
                Container openBag = Inventory.Backpacks.FirstOrDefault(x => x.IsOpen);
                if (openBag != null)
                {
                    noviRing.MoveToContainer(openBag);
                    Chat.WriteLine("Moved to backpack");
                }
            }
            else
            {
                Chat.WriteLine("Can't find item");
            }
        }

        public void dupeStats2(string command, string[] args, ChatWindow window)
        {
            for (int i = 0; i < 200; i++)
            {
                if (Inventory.Find(202742, out Item ring))
                {
                    ring.Equip(EquipSlot.Cloth_RightFinger);

                    Container openBag = Inventory.Backpacks.FirstOrDefault(x => x.IsOpen);
                    
                    if (openBag != null)
                    {
                        ring.MoveToContainer(openBag);
                    }
                    else
                    {
                        Chat.WriteLine("Could not find open bag!");
                    }
                }
                else
                {
                    Chat.WriteLine("Could not find Sanguine Ring for the Infantry Unit");
                }
            }

            Chat.WriteLine("Applied stat 5 times");
        }

        public void dupeStats(string command, string[] args, ChatWindow window)
        {
            if (int.TryParse(args[0], out int itemID))
            {
                if(int.TryParse(args[1], out int numLoops))
                {
                    if (Inventory.Find(itemID, out Item combatLevelThreeItem) && numLoops > 0)
                    {
                        Container openBag = Inventory.Backpacks.FirstOrDefault(x => x.IsOpen);
                        if (openBag != null)
                        {
                            for (int i = 0; i < numLoops; i++)
                            {
                                combatLevelThreeItem.Equip(EquipSlot.Weap_Hud3);
                                combatLevelThreeItem.MoveToContainer(openBag);
                            }
                           
                        }
                    }
                }
                else
                {
                    Chat.WriteLine($"ERROR: Need to specify the number of times to apply /sb {itemID} <numberTimes>");
                }
            }
            else
            {
                Chat.WriteLine("ERROR: Need to specify the item and number of times to apply /sb <itemID> <numberTimes>");
            }
        }
    }
}
